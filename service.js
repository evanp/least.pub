module.exports.get = async (event, context) => {
    const service = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "name": "Least.pub ActivityPub service",
        "type": "Service",
        "id": "https://least.pub/"
    }
    const response = {
        statusCode: 200,
        body: JSON.stringify(service)
    };
    return response;
};
