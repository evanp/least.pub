var AWS = require('aws-sdk')
var Cognito = require('./cognito')
var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.get = async (event, context) => {

    if (!event.pathParameters || !event.pathParameters.username) {
        throw new Error("username required")
    }

    const nickname = event.pathParameters.username;

    const poolID = process.env.USER_POOL_ID;
    const cognito = new Cognito(poolID)

    try {
        const cu = await cognito.getUser(nickname)
        const items = await getOutboxItems(cu.Username)
        const activities = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "OrderedCollection",
            "id": `https://least.pub/${nickname}/activity`,
            "summary": `Actvities by ${nickname}`,
            "published": cu.UserCreateDate,
            "orderedItems": items
        }
        return {
            statusCode: 200,
            body: JSON.stringify(activities)
        }
    } catch (err) {
        if (err.code === "UserNotFoundException") {
            return {
                statusCode: 404,
                body: JSON.stringify(err.message)
            }

        } else {
            return {
                statusCode: 500,
                body: JSON.stringify(err.message)
            }
        }
    }
};

const getOutboxItems = async (nickname) => {
    const outbox = await getOutbox(nickname)
    return getActivities(outbox.outbox)
}

const getOutbox = async (nickname) => {
    return new Promise((resolve, reject) => {
        const params = {
            Key: {
                username: {
                    S: nickname
                }
            },
            TableName: stageName("outboxTable")
        }
        dynamodb.getItem(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(fromItem(data.Item))
            }
        })

    })
}

const getActivities = async (ids) => {
  return new Promise((resolve, reject) => {
    var params = {RequestItems: {}}
    var tableName = stageName('activityTable')
    params.RequestItems[tableName] = {}
    params.RequestItems[tableName].Keys = ids.map((id) => {return {id: {S: id}}})
    dynamodb.batchGetItem(params, (err, data) => {
        if (err) {
            reject(err)
        } else {
            var items = data.Responses[tableName]
            resolve(items.map(fromItem))
        }
    })
  })
}

const stageName = (name) => {
    if (!process.env.STAGE) {
        throw new Error(`No STAGE variable set in environment`)
    }
    return `${name}-${process.env.STAGE}`
}

const toItem = (object) => {
    const item = {}
    for (const key in object) {
        const value = object[key]
        item[key] = toAttributeValue(value)
    }
    return item
}

const fromItem = (object) => {
    const json = {}
    for (const key in object) {
        const value = object[key]
        json[key] = fromAttributeValue(value)
    }
    return json
}

const getObjectKeys = (object) => {
    const keys = []
    for (const key in object) {
        keys.push(key)
    }
    return keys
}

const fromAttributeValue = (value) => {
    const [type] = getObjectKeys(value)
    switch (type) {
        case 'S': {
          return value[type]
          break
        }
        case 'N': {
          return value[type]
          break
        }
        case 'BOOL': {
            return value[type]
            break
        }
        case 'NULL': {
            if (!value[type]) {
                throw new Error("found NULL: false")
            }
            return null
            break
        }
        case 'M': {
            const obj = {}
            for (const key in value[type]) {
              obj[key] = fromAttributeValue(value[type][key])
            }
            return obj
        }
        case 'L': {
            return value[type].map((item) => fromAttributeValue(item))
            break
        }
    }
}

const toAttributeValue = (value) => {
    switch (typeof(value)) {
        case 'number': {
            return {N:value}
            break
        }
        case 'string': {
            return {S: value}
            break
        }
        case 'boolean': {
            return {BOOL: value}
            break
        }
        case 'object': {
            if (value === null) {
                return {NULL: true}
            } else if (Array.isArray(value)) {
                return {L: value.map((item) => toAttributeValue(item))}
            } else {
                const map = {}
                for (const key in value) {
                    map[key] = toAttributeValue(value[key])
                }
                return {M: map}
            }
        }
    }

}
const saveActivity = (activity) => {
    return new Promise((resolve, reject) => {
        const item = {
            Item: toItem(activity),
            ReturnConsumedCapacity: "TOTAL",
            TableName: stageName("activityTable")
        }
        dynamodb.putItem(item, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

const addToOutbox = (nickname, activity) => {
    return new Promise((resolve, reject) => {
        const params = {
            ExpressionAttributeNames: {
             "#OB": "outbox"
            },
            ExpressionAttributeValues: {
             ":a": {
               L: [
                   {S: activity.id}
               ]
              },
              ":empty_list": {
                L: []
              }
            },
            Key: {
             "username": {
               S: nickname
              }
            },
            ReturnValues: "ALL_NEW",
            TableName: stageName('outboxTable'),
            UpdateExpression: "SET #OB = list_append(if_not_exists(#OB, :empty_list), :a)"
        };
        dynamodb.updateItem(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

const distributeActivity = async (activity) => {
    return null
}

exports.post = async (event, context) => {

    if (!event.pathParameters || !event.pathParameters.username) {
        throw new Error("username required")
    }

    const nickname = event.pathParameters.username;

    const poolID = process.env.USER_POOL_ID

    const cognito = new Cognito(poolID)

    try {
        const cu = await cognito.getUser(nickname)
        const token = await cognito.getToken(event)
        const principal = (token) ? await cognito.getPrincipal(token) : null
        if (!principal) {
            throw new Error("Not authenticated")
        }
        if (principal.Username != cu.Username) {
            throw new Error("This is not your outbox")
        }
        // FIXME: wrap with Create activity if not an activity
        const activity = JSON.parse(event.body)
        // FIXME: validate activity
        const uuid = context.invokeid
        activity.id =  `https://least.pub/${nickname}/activity/${uuid}`
        activity.actor = `https://least.pub/${nickname}`
        activity.published = activity.updated = (new Date()).toISOString()
        await saveActivity(activity)
        await addToOutbox(nickname, activity)
        distributeActivity(activity)
        return {
            statusCode: 201,
            headers: {
                "Location": activity.id
            }
        }
    } catch (err) {
        if (err.code === "UserNotFoundException") {
            return {
                statusCode: 404,
                body: JSON.stringify({message: err.message})
            }

        } else {
            return {
                statusCode: 500,
                body: JSON.stringify({message: err.message})
            }
        }
    }
};