var AWS = require('aws-sdk')
var Cognito = require('./cognito')

exports.get = async (event, context) => {
    
    if (!event.pathParameters || !event.pathParameters.username) {
        throw new Error("username required")
    }
    
    const nickname = event.pathParameters.username;
    
    const poolID = process.env.USER_POOL_ID

    const cognito = new Cognito(poolID)
    
    try {
        const cu = await cognito.getUser(nickname)
        const user = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": nickname,
            "type": "Person",
            "id": `https://least.pub/${nickname}`,
            "inbox": `https://least.pub/${nickname}/inbox`,
            "outbox": `https://least.pub/${nickname}/activity`,
            "followers": `https://least.pub/${nickname}/followers`,
            "following": `https://least.pub/${nickname}/following`,
            "liked": `https://least.pub/${nickname}/liked`,
            "published": cu.UserCreateDate,
            "updated": cu.UserLastModifiedDate
        }
        return {
            statusCode: 200,
            body: JSON.stringify(user)
        }
    } catch (err) {
        if (err.code === "UserNotFoundException") {
            return {
                statusCode: 404,
                body: JSON.stringify(err.message)
            }
            
        } else {
            return {
                statusCode: 500,
                body: JSON.stringify(err.message)
            }
        }
    }
};