module.exports.notImplemented = async (event, context) => {
    return {
        statusCode: 501,
        body: JSON.stringify({'message': 'Not yet implemented'})
    }
}

