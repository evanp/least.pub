var assert = require('assert')
var AWS = require('aws-sdk')

class Cognito {

  constructor(poolID) {
    assert(typeof(poolID) == 'string', "poolID parameter must be a string")
    this.poolID = poolID
    this.sp = new AWS.CognitoIdentityServiceProvider({apiVersion: '2016-04-18'});
  }

  getUser(nickname) {
    assert(typeof(nickname) == 'string', "poolID parameter must be a string")
    return new Promise((resolve, reject) => {
        assert(typeof(this.poolID) === 'string')
        const params = {
          UserPoolId: this.poolID,
          Username: nickname
        }
        this.sp.adminGetUser(params, (err, results) => {
            if (err) {
                reject(err)
            } else {
                resolve(results)
            }
        })
    })
  }

  getPrincipal(token) {
    assert(typeof(token) === 'string', "token must be a string")
    assert(token.match(/^[A-Za-z0-9-_=.]+$/), "token must contain only letters, numbers, dash, underscore, period, equals")
    return new Promise((resolve, reject) => {
      this.sp.getUser({"AccessToken": token}, (err, data) => {
          if (err) {
              reject(err)
          } else {
              resolve(data)
          }
      })
    })
  }

  getToken(event) {
    assert(typeof(event) === 'object', 'event must be an object')
    if (event.headers && event.headers.Authorization) {
      const match = event.headers.Authorization.match(/^Bearer (\S+)$/)
      return (match) ? match[1] : null
    } else if (event.queryStringParameters && event.queryStringParameters.access_token) {
      return event.queryStringParameters.access_token
    } else {
      return null
    }
  }
}

module.exports = Cognito
